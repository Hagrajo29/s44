// placeholder database: https://jsonplaceholder.typicode.com/posts
// fetch() - used to perform crud operations in a given url
		// accepts 2 arguments - url (needed) and options (not a requirement) 
			// options parameter - used only when the dev needs the details of the request from the user

// get the post data
/*
	use fetch method to get the posts inside the "https://jsonplaceholder.typicode.com/posts"
		make the reponse in json format (.then)
		log the response the console (.then)
*/
fetch("https://jsonplaceholder.typicode.com/posts")
.then(response =>response.json())
.then(data => showPost(data));


// add post
document.querySelector("#form-add-post").addEventListener('submit',(e)=>{
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: "POST",
		body: JSON.stringify({
			title:document.querySelector("#txt-title").value,
			body:document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers:{"Content-Type": "application/json; charset=UTF-8"}
	})
	.then((response) => response.json())//converts the response into JSON format
	.then(data => {
		// showPost([data]); - to show/display the created post, but the database itself will not be shown

		alert("Post Created Successfully")

		// to claer the text in the input fields upon creating a post
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})
})

/*
	create a showPost function that will display the posts in the placeholder database as well as the posts created in the webpage (priority - to display the created posts)
		the posts should apear in the div under the "Posts"

		send a screenshot of the output in the batch hangouts/google chat
*/

/*since the database is a placeholder and cannot be updated, the created post cannot be displayed in the frontend. However, with the use of a database that can be updated, all created objects can be displayed (as long as the URL is correct)*/
const showPost = (posts) =>{
	let postEntries = [];

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// edit post function

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	// removeAttribute - to remove the attribute from the element; it receives a string argument that serves to be the attribute of the element
	document.querySelector("#btn-submit-update").removeAttribute('disabled');
}

// update post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault()
	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers:{"Content-Type": "application/json; charset=UTF-8"}
	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert('Post Successfully Updated');

		// clears the input fields
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		// setAttribute - sets the attribute of the element; accepts two argumets
				// string - the attribute to be set
				// boolean - to be set into true/false
		document.querySelector("#btn-submit-update").setAttribute('disabled', true)
	})
})

/*
	ACTIVITY: make the "Delete" button work
		only the post which delete button is pressed should deleted

		hint: could be a 3-line code snippet
*/

const deletePost = (id) => {
	document.querySelector(`#post-${id}`).remove();
}
